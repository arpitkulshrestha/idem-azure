import copy
from collections import ChainMap

import dict_tools.differ as differ
import pytest


RESOURCE_NAME = "my-resource"
RESOURCE_GROUP_NAME = "my-resource-group"
NIC_NAME = "my-nic"
RESOURCE_PARAMETERS = {
    "location": "eastus",
    "tags": {"tag-key": "tag-value"},
    "ip_configurations": [
        {
            "name": "my-nic-ipc",
            "private_ip_address": "10.0.1.1",
            "private_ip_address_allocation": "Dynamic",
            "subnet_id": "subnet_id",
            "primary": "true",
            "private_ip_address_version": "IPv4",
        }
    ],
}


RESOURCE_PARAMETERS_RAW = {
    "location": "eastus",
    "tags": {"tag-key": "tag-value"},
    "properties": {
        "ipConfigurations": [
            {
                "name": "my-nic-ipc",
                "properties": {
                    "privateIPAddress": "10.0.1.1",
                    "privateIPAllocationMethod": "Dynamic",
                    "subnet": {
                        "id": "subnet_id",
                    },
                    "primary": "true",
                    "privateIPAddressVersion": "IPv4",
                },
            }
        ]
    },
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of network interfaces. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.network.network_interfaces.present = (
        hub.states.azure.network.network_interfaces.present
    )
    mock_hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present = (
        hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present
    )
    mock_hub.tool.azure.network.network_interfaces.convert_present_to_raw_network_interfaces = (
        hub.tool.azure.network.network_interfaces.convert_present_to_raw_network_interfaces
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.network_interfaces.get = (
        hub.exec.azure.network.network_interfaces.get
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkInterfaces/{NIC_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.network.network_interfaces",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NIC_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NIC_NAME in url
        assert not differ.deep_diff(RESOURCE_PARAMETERS_RAW, json)
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await mock_hub.states.azure.network.network_interfaces.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NIC_NAME,
        **RESOURCE_PARAMETERS,
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.network.network_interfaces '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.network_interfaces.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NIC_NAME,
        **RESOURCE_PARAMETERS,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.network.network_interfaces '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of network interface. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.network.network_interfaces.present = (
        hub.states.azure.network.network_interfaces.present
    )
    mock_hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present = (
        hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present
    )
    mock_hub.tool.azure.network.network_interfaces.convert_present_to_raw_network_interfaces = (
        hub.tool.azure.network.network_interfaces.convert_present_to_raw_network_interfaces
    )
    mock_hub.tool.azure.network.network_interfaces.update_network_interfaces_payload = (
        hub.tool.azure.network.network_interfaces.update_network_interfaces_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.network_interfaces.get = (
        hub.exec.azure.network.network_interfaces.get
    )
    resource_parameters_update = {
        "ip_configurations": [
            {
                "name": "my-nic-ipc-updated",
                "private_ip_address": "10.0.1.8",
                "private_ip_address_allocation": "Static",
                "subnet_id": "subnet_id_updated",
                "primary": "true",
                "private_ip_address_version": "IPv4",
            }
        ],
        "location": "eastus",
        "tags": {"tag-new-key": "tag-new-value"},
    }

    resource_parameters_update_raw = {
        "properties": {
            "ipConfigurations": [
                {
                    "name": "my-nic-ipc-updated",
                    "properties": {
                        "privateIPAddress": "10.0.1.8",
                        "privateIPAllocationMethod": "Static",
                        "subnet": {
                            "id": "subnet_id_updated",
                        },
                        "primary": "true",
                        "privateIPAddressVersion": "IPv4",
                    },
                }
            ]
        },
        "location": "eastus",
        "tags": {"tag-new-key": "tag-new-value"},
    }

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkInterfaces/{NIC_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkInterfaces/{NIC_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NIC_NAME in url
        assert resource_parameters_update_raw["properties"] == json.get("properties")
        assert resource_parameters_update_raw["location"] == json.get("location")
        assert resource_parameters_update_raw["tags"] == json.get("tags")
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.network_interfaces.present(
        test_ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NIC_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.network.network_interfaces '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )

    # Turn on put.side_effect after testing --test, since PUT operation should not be called in --test
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    # Test present() with --test flag off
    ret = await mock_hub.states.azure.network.network_interfaces.present(
        ctx,
        RESOURCE_NAME,
        RESOURCE_GROUP_NAME,
        NIC_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of network interfaces. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.network.network_interfaces.absent = (
        hub.states.azure.network.network_interfaces.absent
    )
    mock_hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present = (
        hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.network_interfaces.get = (
        hub.exec.azure.network.network_interfaces.get
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NIC_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.network.network_interfaces.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NIC_NAME
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.network.network_interfaces '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of network interfaces. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.network.network_interfaces.absent = (
        hub.states.azure.network.network_interfaces.absent
    )
    mock_hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present = (
        hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    mock_hub.exec.azure.network.network_interfaces.get = (
        hub.exec.azure.network.network_interfaces.get
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
            f"/providers/Microsoft.Network/networkInterfaces/{NIC_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ctx.acct.subscription_id in url
        assert RESOURCE_GROUP_NAME in url
        assert NIC_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get

    # Test absent() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.network.network_interfaces.absent(
        test_ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NIC_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.network.network_interfaces '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    # Test absent() with --test flag off
    ret = await mock_hub.states.azure.network.network_interfaces.absent(
        ctx, RESOURCE_NAME, RESOURCE_GROUP_NAME, NIC_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.network.network_interfaces '{RESOURCE_NAME}'" in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of network interfaces.
    """
    mock_hub.states.azure.network.network_interfaces.describe = (
        hub.states.azure.network.network_interfaces.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present = (
        hub.tool.azure.network.network_interfaces.convert_raw_network_interfaces_to_present
    )
    mock_hub.exec.azure.network.network_interfaces.list = (
        hub.exec.azure.network.network_interfaces.list
    )

    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{RESOURCE_GROUP_NAME}"
        f"/providers/Microsoft.Network/networkInterfaces/{NIC_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.network.network_interfaces.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.network.network_interfaces.present" in ret_value.keys()
    described_resource = ret_value.get("azure.network.network_interfaces.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
    )


def check_returned_states(old_state, new_state, expected_old_state, expected_new_state):
    if old_state:
        assert RESOURCE_GROUP_NAME == old_state.get("resource_group_name")
        assert NIC_NAME == old_state.get("network_interface_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["ip_configurations"] == old_state.get(
            "ip_configurations"
        )
        assert expected_old_state["tags"] == old_state.get("tags")
    if new_state:
        assert RESOURCE_GROUP_NAME == new_state.get("resource_group_name")
        assert NIC_NAME == new_state.get("network_interface_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["ip_configurations"] == new_state.get(
            "ip_configurations"
        )
        assert expected_new_state["tags"] == new_state.get("tags")
