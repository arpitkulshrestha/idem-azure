"""
Integration tests for rest.py
"""


def test_context(hub):
    ...


def test_parse(hub, url, desc):
    """
    Test the parse function for completion and expected return.
    :param hub:
    :param url:
    :return:
    """
    m = hub.pop_create.azure.rest.parse(url, desc)
    assert m == {
        "aks": [
            {
                "resource": "managed-clusters",
                "create": "https://docs.microsoft.com/en-us/rest/api/aks//managed-clusters/create-or-update",
                "delete": "https://docs.microsoft.com/en-us/rest/api/aks//managed-clusters/delete",
                "get": "https://docs.microsoft.com/en-us/rest/api/aks//managed-clusters/get",
                "list": "https://docs.microsoft.com/en-us/rest/api/aks//managed-clusters/list",
                "update": "https://docs.microsoft.com/en-us/rest/api/aks//managed-clusters/update-tags",
            }
        ]
    }
